﻿using System;
using System.Collections.Generic;

namespace Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        static void ShowcaseCollections()
        {
            // 1D string array
            string[] myStringArray = new string[10];
            myStringArray[0] = "Hello";

            // 1D int array
            int[] myIntArray = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            // 2D Array
            int[,] my2Darray = new int[,] { { 1, 2 }, { 3, 4 }, { 5, 6 } };

            // List
            List<string> daysOfTheWeek = new List<string>() { "Monday", "Tuesday" };
            daysOfTheWeek.Add("Wednesday");
            daysOfTheWeek.Remove("Tuesday");

            // Queue
            Queue<int> customers = new Queue<int>();
            customers.Enqueue(10);
            customers.Enqueue(42);
            customers.Dequeue();

            // Stack
        }
    }
}
